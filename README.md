# Path Integral Monte Carlo methods for a chain of coupled oscillators

## Development Process

Simply build and choose an example to run.

```
# Clone the repo:
git clone https://gitlab.com/path_integral_mc/pimc_chain.git
cd pimc_chain

# Build:
make

# Run:
cd build
./print_chain
```

## License

Copyright (C) 2021 Purrello V. H.

PIMC Chain is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

PIMC Chain is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You have a copy of the GNU General Public License in the file [COPYING](COPYING). You can also see [GPL 3.0+](http://www.gnu.org/licenses/gpl-3.0.en.html).
