# Path Integral Monte Carlo methods for a chain of coupled oscillators
# Copyright (C) 2021 Purrello V. H.

# This file is part of PIMC_Chain.

# PIMC_Chain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# PIMC_Chain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with PIMC_Chain. If not, see <http://www.gnu.org/licenses/>

SHELL			= /bin/sh

# Paths & Includes
OUTDIR			= $(CURDIR)/build
SRCDIR			= $(CURDIR)/src
EXAMPLESDIR		= $(CURDIR)/examples
EXTDIR			= $(CURDIR)/external
VPATH			= $(SRCDIR):$(EXAMPLESDIR)
INCLUDES		= -I$(SRCDIR) -I$(EXTDIR)/HighFive/include -I/usr/include/hdf5/serial

LDFLAGS			=
LDLIBS			= -lfftw3 -lm -lhdf5_serial
WARN			= -Wall -Wextra
OPTIMIZATION		= -O3 -march=native
CXX			= g++
CPPFLAGS		= $(INCLUDES)
CXXFLAGS		= $(OPTIMIZATION) $(WARN) -std=c++17
DEPFLAGS		= -MT $@ -MMD -MP -MF $(OUTDIR)/$*.d
COMPILE.cc		= $(CXX) $(DEPFLAGS) $(CXXFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c

SOURCES_SRC		= $(shell cd $(SRCDIR) && ls *.cpp)
EXAMPLES_SRC		= $(shell cd $(EXAMPLESDIR) && ls *.cpp)
EXAMPLES		= $(EXAMPLES_SRC:.cpp=)
DEPS			:= $(SOURCES_SRC:%.cpp=$(OUTDIR)/%.d) $(EXAMPLES_SRC:%.cpp=$(OUTDIR)/%.d)
# DEPS is explained in https://make.mad-scientist.net/papers/advanced-auto-dependency-generation/

# Rules
.PHONY: all clean

all: $(patsubst %, $(OUTDIR)/%, $(EXAMPLES))

$(DEPS):
include $(wildcard $(DEPS))

$(OUTDIR)/%.o : %.cpp $(OUTDIR)/%.d | $(OUTDIR)
	$(COMPILE.cc) -o $@ $<

$(OUTDIR): ; @mkdir -p $@

clean:
	rm -fv $(OUTDIR)/*.d $(OUTDIR)/*.o $(patsubst %, $(OUTDIR)/%, $(EXAMPLES))
	rmdir $(OUTDIR)

$(OUTDIR)/print_chain: $(patsubst %, $(OUTDIR)/%, print_chain.o chain.o)
	$(LINK.cc) -o $@ $^ $(LDLIBS)

$(OUTDIR)/chain_corr: $(patsubst %, $(OUTDIR)/%, chain_corr.o chain.o)
	$(LINK.cc) -o $@ $^ $(LDLIBS)

$(OUTDIR)/mc_anharmonic: $(patsubst %, $(OUTDIR)/%, mc_anharmonic.o chain.o anharmonic_chain.o)
	$(LINK.cc) -o $@ $^ $(LDLIBS)

$(OUTDIR)/chain_energy: $(patsubst %, $(OUTDIR)/%, chain_energy.o chain.o anharmonic_chain.o)
	$(LINK.cc) -o $@ $^ $(LDLIBS)

$(OUTDIR)/reweighting: $(patsubst %, $(OUTDIR)/%, reweighting.o chain.o anharmonic_chain.o)
	$(LINK.cc) -o $@ $^ $(LDLIBS)

$(OUTDIR)/cumulants: $(patsubst %, $(OUTDIR)/%, cumulants.o chain.o anharmonic_chain.o)
	$(LINK.cc) -o $@ $^ $(LDLIBS)
