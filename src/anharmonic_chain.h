/*  Path Integral Monte Carlo methods for a chain of coupled oscillators
    Copyright (C) 2021 Purrello V. H.

    This file is part of PIMC_Chain.

    PIMC_Chain is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PIMC_Chain is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PIMC_Chain. If not, see <http://www.gnu.org/licenses/> */

#ifndef ANHARMONIC_CHAIN_H_14899
#define ANHARMONIC_CHAIN_H_14899

#include "chain.h"

class AChain: public Chain
{
    public:
        AChain (double _beta, size_t _N, bool _VERBOSE=true, double _dtau=0.1, double _omega0=1.0, double _anh=1.0);
	void build_paths_for_A() override;
	void set_A(size_t m, size_t k, size_t i, double value) override;
	double calc_Ep_m(size_t m) override;
	double calc_Ep_anh();
	double calc_Ep_anh_m(size_t m);
	double calc_Ep_from_A() = delete; // deleted since it's incorrect for the anharmonic chain
	double calc_Ep_from_A(size_t k) = delete; // deleted since it's incorrect for the anharmonic chain
	double get_Ep_anh();
	double get_Ep_anh_m(size_t m);
    private:
	inline double _calc_Ep_m(size_t m, bool harmflag, bool anhflag);

	double anh;
	double Ep_anh_total;
	RVector Ep_anh_total_m;
};
#endif /* ANHARMONIC_CHAIN_H_14899 */
