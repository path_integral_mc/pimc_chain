/*  Path Integral Monte Carlo methods for a chain of coupled oscillators
    Copyright (C) 2021 Purrello V. H.

    This file is part of PIMC_Chain.

    PIMC_Chain is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PIMC_Chain is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PIMC_Chain. If not, see <http://www.gnu.org/licenses/> */

#include "chain.h"

#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <fstream>
#include <algorithm>

Chain::Chain(double _beta, size_t _N, bool _VERBOSE, double _dtau, double _omega0, size_t _nk) :
    gen(rd()), beta(_beta), N(_N), M(_beta/_dtau), VERBOSE(_VERBOSE),
    dtau(_dtau), omega0(_omega0),
    x(M, RVector(_N)), A(M, CVector(_N/2+1)), Ep_total_m(M),
    p_r2c(M), p_c2r(M), k_for_C(N>2*_nk ? _nk : _N/2),
    C(M, CVector(N>2*_nk ? _nk : _N/2)), C_mean(M, CVector(N>2*_nk ? _nk : _N/2))
{
    if (N%2)
	std::cerr << "N must be even, continue at your own risk" << std::endl;

    // Fix max number of k for calculating corr function to _nk
    for (size_t i = 0; i < k_for_C.size(); ++i)
	if (N > 2*_nk)
	    k_for_C[i] = static_cast<size_t>((i+1) * N/(2*_nk));
	else
	    k_for_C[i] = i+1;

    if (VERBOSE) {
        std::cerr << "# Number of elements N = " << N << '\n'
	    << "# Number of slices M = " << M << '\n'
	    << "# beta = " << beta << '\n'
	    << "# dtau = " << dtau << '\n'
	    << "# omega_0 = " << omega0 << '\n'
	    << "# Max number of k for corr = " << _nk << std::endl;
    }

    if (VERBOSE)
        std::cerr << "# Creating plans" << std::endl;
    for (size_t i=0; i<p_r2c.size(); ++i)
	p_r2c[i] = fftw_plan_dft_r2c_1d(N, x[i].data(),
		reinterpret_cast<fftw_complex*>(A[i].data()), FFTW_ESTIMATE);
    for (size_t i=0; i<p_c2r.size(); ++i)
	p_c2r[i] = fftw_plan_dft_c2r_1d(N,
		reinterpret_cast<fftw_complex*>(A[i].data()), x[i].data(), FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
}

Chain::~Chain()
{
    for (auto &p : p_r2c)
	fftw_destroy_plan(p);
    for (auto &p : p_c2r)
	fftw_destroy_plan(p);
}

void Chain::init_A()
{
    auto &A0 = A.front();
    const size_t A0_size = A0.size();
    for (size_t k=1; k < A0_size-1; ++k) {
	const double wk = 2*omega0*sin(k*M_PI/N);
	const double sigma = std::sqrt(N/(4*wk*tanh(wk*beta/2.)));
	std::normal_distribution<> gauss{0, sigma};
	A0[k].real(gauss(gen));
	A0[k].imag(gauss(gen));
    }
    {
	auto k = A0_size-1; // => k == N/2
	const double wk = 2*omega0*sin(k*M_PI/N);
	const double sigma = std::sqrt(N/(2*wk*tanh(wk*beta/2.)));
	std::normal_distribution<> gauss{0, sigma};
	A0.back().real(gauss(gen));
	A0.back().imag(0);
    }
}

void Chain::build_paths_for_A()
{
    const auto Am_last = A.front().size()-1; // => Am_last == N/2

    CVector Al(A.front());

    for (size_t m = 1; m < M; ++m) {
	// Exclude A_0 and A_last from the loop
	for (size_t k=1; k<Am_last; ++k) {
	    const double wk = 2*omega0*sin(k*M_PI/N);
	    const double sinh_dtau = std::sinh(wk*dtau);
	    const double coth_dtau = std::cosh(wk*dtau) / sinh_dtau;

	    const double sinh_last = std::sinh(wk*(M-m)*dtau);
	    const double coth_last = std::cosh(wk*(M-m)*dtau) / sinh_last;
	    const double gamma1 = coth_dtau + coth_last;

	    for (size_t i=0; i<2; ++i) {
		const double gamma2 = reinterpret_cast<double(&)[2]>(A[m-1][k])[i] / sinh_dtau
		    + reinterpret_cast<double(&)[2]>(Al[k])[i] / sinh_last;
		const double mean_xk = gamma2 / gamma1;
		const double sigma = 1. / std::sqrt(wk*gamma1*2/N);
		std::normal_distribution<> gauss{mean_xk, sigma};
		reinterpret_cast<double(&)[2]>(A[m][k])[i] = gauss(gen);
	    }
	}
	// Build only A_last.real
	{
	    const size_t k = Am_last;
	    const size_t i = 0;
	    const double wk = 2*omega0*sin(k*M_PI/N);
	    const double sinh_dtau = std::sinh(wk*dtau);
	    const double coth_dtau = std::cosh(wk*dtau) / sinh_dtau;

	    const double sinh_last = std::sinh(wk*(M-m)*dtau);
	    const double coth_last = std::cosh(wk*(M-m)*dtau) / sinh_last;
	    const double gamma1 = coth_dtau + coth_last;

	    const double gamma2 = reinterpret_cast<double(&)[2]>(A[m-1][k])[i] / sinh_dtau
		+ reinterpret_cast<double(&)[2]>(Al[k])[i] / sinh_last;
	    const double mean_xk = gamma2 / gamma1;
	    const double sigma = 1. / std::sqrt(wk*gamma1/N);
	    std::normal_distribution<> gauss{mean_xk, sigma};
	    reinterpret_cast<double(&)[2]>(A[m][k])[i] = gauss(gen);
	}
    }

    A_to_x();
    calc_Ep();
}

void Chain::direct_sampling_new_A(size_t m, size_t k, size_t i)
{
    const double wk = 2*omega0*sin(k*M_PI/N);
    const double sinh_dtau = std::sinh(wk*dtau);
    const double coth_dtau = std::cosh(wk*dtau) / sinh_dtau;

    const double gamma1 = 2 * coth_dtau;
    const double gamma2 = get_A((M+m-1)%M, k, i) / sinh_dtau + get_A((m+1)%M, k, i) / sinh_dtau;
    const double mean_xk = gamma2 / gamma1;
    double sigma;
    if (k == N/2)
	sigma = 1. / std::sqrt(wk*gamma1/N);
    else
	sigma = 1. / std::sqrt(wk*gamma1*2/N);

    std::normal_distribution<> gauss{mean_xk, sigma};
    set_A(m, k, i, gauss(gen));
}

RMatrix Chain::get_x()
{
    return x;
}

CMatrix Chain::get_A()
{
    return A;
}

double Chain::get_A(size_t m, size_t k, size_t i)
{
    return reinterpret_cast<double(&)[2]>(A.at(m).at(k))[i];
}

void Chain::set_A(size_t m, size_t k, size_t i, double value)
{
    reinterpret_cast<double(&)[2]>(A.at(m).at(k))[i] = value;
    const double Eold = get_Ep_m(m);
    A_to_x(m);
    const double Enew = calc_Ep_m(m);
    Ep_total += (Enew - Eold) / M;
}

void Chain::A_to_x()
{
    for (auto &p : p_c2r)
	fftw_execute(p);

    const double norm = 1./N;
    for (auto &xm : x)
	for (auto &xi : xm)
	    xi *= norm;
}

void Chain::A_to_x(size_t m)
{
    fftw_execute(p_c2r.at(m));

    const double norm = 1./N;
    for (auto &xi : x.at(m))
	xi *= norm;
}

void Chain::x_to_A()
{
    for (auto &p : p_r2c)
	fftw_execute(p);
}

void Chain::x_to_A(size_t m)
{
    fftw_execute(p_r2c.at(m));
}

void Chain::print_A()
{
    for (const auto &Am : A) {
	for (const auto &Aq : Am)
	    std::cout << Aq.real() << '\t' << Aq.imag() << '\n';
	std::cout << std::endl;
    }
}

void Chain::print_x()
{
    for (const auto &xm : x) {
	for (const auto &xi : xm)
	    std::cout << xi << '\n';
	std::cout << std::endl;
    }
}

double Chain::get_Ep()
{
    return Ep_total;
}

double Chain::get_Ep_m(size_t m)
{
    return Ep_total_m.at(m);
}

double Chain::calc_Ep()
{
    double Ep = 0.0;
    for (size_t m = 0; m < M; ++m)
	Ep += calc_Ep_m(m);
    Ep_total = Ep/M;
    return Ep_total;
}

double Chain::calc_Ep_m(size_t m)
{
    const double harm_pref = 0.5 * omega0 * omega0;
    const auto xm = x.at(m);
    double Ep_slice = 0;
    for (size_t n = 0; n < N-1; ++n) {
	const auto dx = xm[n+1]-xm[n];
	Ep_slice += dx*dx;
    }
    const auto dx = xm[0]-xm[N-1];
    Ep_slice += dx*dx;
    Ep_slice *= harm_pref;
    Ep_total_m.at(m) = Ep_slice;
    return Ep_slice;
}

double Chain::calc_Ep_from_A()
{
    double Ep = 0;
    size_t m = 0;
    for (const auto &Am : A) {
	double Ep_slice = 0;
	for (size_t k = 1; k < N/2; ++k) {
	    const double wk = 2*omega0*sin(k*M_PI/N);
	    // 2 times because it also takes into account the complex conjugates
	    Ep_slice += 2 * wk * wk * std::norm(Am[k]);
	}
	{
	    const double wk = 2*omega0*sin(M_PI/2);
	    Ep_slice += wk * wk * std::norm(Am[N/2]);
	}
	m++;
	Ep += (Ep_slice-Ep) / m;
    }
    return (0.5/N) * Ep;
}

double Chain::calc_Ep_from_A(size_t k)
{
    if (k>N/2) {
	std::cerr << "k = " << k << " is out of bounds in calc_Ep_from_A(k)" << std::endl;
	return -1;
    }
    double Ep = 0;
    size_t m = 0;
    for (const auto &Am : A) {
	double Ep_slice = 0;
	const double wk = 2*omega0*sin(k*M_PI/N);
	Ep_slice += wk * wk * std::norm(Am[k]);
	m++;
	Ep += (Ep_slice-Ep) / m;
    }
    return (0.5/N) * Ep;
}

CMatrix Chain::calc_corr_from_A_fast()
{
    const auto A0 = A.front();
    size_t m = 0;
    for (const auto &Am : A) {
	for (size_t ik = 0; ik < k_for_C.size(); ++ik) {
	    const size_t k = k_for_C[ik];
	    C[m][ik] = Am[k] * std::conj(A0[k]);
	}
	++m;
    }
    return C;
}

CMatrix Chain::calc_corr_from_A()
{
    for (size_t ik = 0; ik < k_for_C.size(); ++ik) {
	const size_t k = k_for_C[ik];
	CVector conj_Ak(M);
	for (size_t a = 0; a < M; ++a)
	    conj_Ak[a] = std::conj(A[a][k]);

	for (size_t m = 0; m < M; ++m) {
	    std::complex<double> Cm_k = 0.;
	    for (size_t a = 0; a < M; ++a) {
		const std::complex<double> new_Cm_k = A[(a+m)%M][k] * conj_Ak[a];
		const std::complex<double> dCm_k = new_Cm_k - Cm_k;
		Cm_k += dCm_k / static_cast<double>(a+1);
	    }
	    C[m][ik] = Cm_k;
	}
    }
    return C;
}

IVector Chain::get_k_for_corr()
{
    return k_for_C;
}

CMatrix Chain::get_corr()
{
    return C;
}

void Chain::reset_accumulators()
{
    realizations = 0;
    Ep_mean = 0;
    Ep_sigma2 = 0;
    Ep_sigma2_averaged = false;
    for (auto &Cm : C_mean)
	std::fill(Cm.begin(), Cm.end(), 0);
}

void Chain::update_mean_Ep()
{
    ++realizations;
    subroutine_update_mean_Ep();
}

void Chain::update_mean_corr()
{
    ++realizations;
    subroutine_update_mean_corr();
}

void Chain::update_mean_Ep_and_corr()
{
    ++realizations;
    subroutine_update_mean_Ep();
    subroutine_update_mean_corr();
}

std::tuple<double, double> Chain::get_mean_Ep()
{
    if (! Ep_sigma2_averaged) {
	Ep_sigma2 /= realizations;
	Ep_sigma2_averaged = true;
    }
    return {Ep_mean, Ep_sigma2};
}

CMatrix Chain::get_mean_corr()
{
    return C_mean;
}

void Chain::print_mean_Ep()
{
    if (! Ep_sigma2_averaged) {
	Ep_sigma2 /= realizations;
	Ep_sigma2_averaged = true;
    }
    const auto Ep_err = std::sqrt(Ep_sigma2/realizations);
    std::cout << beta << '\t' << Ep_mean << '\t' << Ep_err << std::endl;
}

void Chain::print_mean_corr()
{
    std::cout << '#';
    for (const auto &k : k_for_C)
	std::cout << "C[" << k << "].R\tC[" << k << "].I\t";
    std::cout << '\n';
    for (const auto &Cm : C_mean) {
	for (const auto &Ck : Cm)
	    std::cout << Ck.real() << '\t' << Ck.imag() << '\t';
	std::cout << '\n';
    }
    std::cout << std::endl;
}

inline void Chain::subroutine_update_mean_Ep()
{
    // https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Welford's_online_algorithm
    const double new_Ep = get_Ep();
    const double dEp = new_Ep - Ep_mean;
    Ep_mean += dEp / realizations;
    const double dEpn = new_Ep - Ep_mean;
    Ep_sigma2 += dEp * dEpn;
}

inline void Chain::subroutine_update_mean_corr()
{
    calc_corr_from_A();
    CMatrix dC(M, CVector(k_for_C.size()));
    for (size_t m = 0; m < M; ++m) {
	std::transform (
	    C[m].begin(), C[m].end(),
	    C_mean[m].begin(),
	    dC[m].begin(),
	    std::minus<>()
	    );
	std::transform (
	    C_mean[m].begin(), C_mean[m].end(),
	    dC[m].begin(),
	    C_mean[m].begin(),
	    [=](std::complex<double> c, std::complex<double> dc) -> std::complex<double>
	    {
		return c + dc/static_cast<double>(realizations);
	    }
	    );
    }
}
