/*  Path Integral Monte Carlo methods for a chain of coupled oscillators
    Copyright (C) 2021 Purrello V. H.

    This file is part of PIMC_Chain.

    PIMC_Chain is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PIMC_Chain is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PIMC_Chain. If not, see <http://www.gnu.org/licenses/> */

#include "anharmonic_chain.h"

#include <iostream>

AChain::AChain(double _beta, size_t _N, bool _VERBOSE, double _dtau, double _omega0, double _anh) :
    Chain(_beta, _N, _VERBOSE, _dtau, _omega0), anh(_anh), Ep_anh_total_m(M)
{
    if (VERBOSE)
        std::cerr << "# anh = " << anh << std::endl;
    if (anh == 0)
	std::cerr << "# Use the harmonic chain in chain.h to get better performance" << std::endl;
}

void AChain::build_paths_for_A()
{
    Chain::build_paths_for_A();
    calc_Ep_anh();
}

void AChain::set_A(size_t m, size_t k, size_t i, double value)
{
    const double Eanh_old = get_Ep_anh_m(m);
    Chain::set_A(m, k, i, value);
    const double Eanh_new = calc_Ep_anh_m(m);
    Ep_anh_total += (Eanh_new - Eanh_old) / M;
}

double AChain::calc_Ep_m(size_t m)
{
    Ep_total_m.at(m) = _calc_Ep_m(m, true, true);
    return Ep_total_m.at(m);
}

double AChain::calc_Ep_anh()
{
    double Ep = 0.0;
    for (size_t m = 0; m < M; ++m)
	Ep += calc_Ep_anh_m(m);
    /*
    double Ep = std::reduce(
	Ep_anh_total_m.begin(), Ep_anh_total_m.end(),
	0.0
	);
    */
    /*
    std::vector<size_t> mvalues(M);
    std::iota(mvalues.begin(), mvalues.end(), 0);
    double Ep = std::transform_reduce(
	    mvalues.begin(), mvalues.end(),
	    0.0,
	    std::plus<>(),
	    [=](auto m) { return calc_Ep_anh_m(m); }
	    );
    */
    Ep_anh_total = Ep/M;
    return Ep_anh_total;
}

double AChain::calc_Ep_anh_m(size_t m)
{
    Ep_anh_total_m.at(m) = _calc_Ep_m(m, false, true);
    return Ep_anh_total_m.at(m);
}

double AChain::get_Ep_anh()
{
    return Ep_anh_total;
}

double AChain::get_Ep_anh_m(size_t m)
{
    return Ep_anh_total_m.at(m);
}

inline double AChain::_calc_Ep_m(size_t m, bool harmflag, bool anhflag)
{
    const double harm_pref = 0.5 * omega0 * omega0 * harmflag;
    const double anh_pref = 0.25 * anh * anhflag;
    const auto xm = x.at(m);
    double Ep_slice = 0;
    for (size_t n = 0; n < N-1; ++n) {
	const auto dx = xm[n+1]-xm[n];
	Ep_slice += harm_pref * dx*dx + anh_pref * dx*dx*dx*dx;
    }
    const auto dx = xm[0]-xm[N-1];
    Ep_slice += harm_pref * dx*dx + anh_pref * dx*dx*dx*dx;
    return Ep_slice;
}
