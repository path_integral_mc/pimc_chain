/*  Path Integral Monte Carlo methods for a chain of coupled oscillators
    Copyright (C) 2021 Purrello V. H.

    This file is part of PIMC_Chain.

    PIMC_Chain is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PIMC_Chain is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PIMC_Chain. If not, see <http://www.gnu.org/licenses/> */

#ifndef CHAIN_H_19511
#define CHAIN_H_19511

#include <vector>
#include <random>
#include <complex> //Must be included before fftw3
#include <fftw3.h>
#include <tuple>

using IVector = std::vector<size_t>;
using RVector = std::vector<double>;
using RMatrix = std::vector<RVector>;
using CVector = std::vector<std::complex<double> >;
using CMatrix = std::vector<CVector>;

class Chain
{
    public:
        Chain (double _beta, size_t _N, bool _VERBOSE=true, double _dtau=0.1, double _omega0=1.0, size_t _nk=32);
	virtual ~Chain ();
        void init_A();
        virtual void build_paths_for_A();
	void direct_sampling_new_A(size_t m, size_t k, size_t i);
	RMatrix get_x();
	CMatrix get_A();
	double get_A(size_t m, size_t k, size_t i);
	virtual void set_A(size_t m, size_t k, size_t i, double value);
        void A_to_x();
        void A_to_x(size_t m);
        void x_to_A();
        void x_to_A(size_t m);
	void print_A();
	void print_x();
	virtual double calc_Ep();
	virtual double calc_Ep_m(size_t m);
	double calc_Ep_from_A();
	double calc_Ep_from_A(size_t k);
	double get_Ep();
	double get_Ep_m(size_t m);
	CMatrix calc_corr_from_A_fast();
	CMatrix calc_corr_from_A();
	IVector get_k_for_corr();
	CMatrix get_corr();
	void reset_accumulators();
	void update_mean_Ep();
	void update_mean_corr();
	void update_mean_Ep_and_corr();
	std::tuple<double, double> get_mean_Ep();
	CMatrix get_mean_corr();
	void print_mean_Ep();
	void print_mean_corr();

    protected:
	inline void subroutine_update_mean_Ep();
	inline void subroutine_update_mean_corr();

	std::random_device rd;  //Will be used to obtain a seed for the random number engine
	std::mt19937 gen; //Standard mersenne_twister_engine seeded with rd()
	const double beta;
        const size_t N; //number of elements in chain
        const size_t M; //number of slices
        const bool VERBOSE;
	const double dtau;
	const double omega0;
	RMatrix x; //x coordinates
	CMatrix A; //normal modes
	double Ep_total; //Energy
	RVector Ep_total_m; //Energy per slice
	std::vector<fftw_plan> p_r2c, p_c2r;
	//accumulators
	size_t realizations = 0;
	double Ep_mean = 0;
	double Ep_sigma2 = 0;
	bool Ep_sigma2_averaged = false;
	IVector k_for_C;
	CMatrix C;
	CMatrix C_mean;
};
#endif /* CHAIN_H_19511 */
