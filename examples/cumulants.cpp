/*  Path Integral Monte Carlo methods for a chain of coupled oscillators
    Copyright (C) 2021 Purrello V. H.

    This file is part of PIMC_Chain.

    PIMC_Chain is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PIMC_Chain is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PIMC_Chain. If not, see <http://www.gnu.org/licenses/> */

#include "anharmonic_chain.h"
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <tuple>
#include <algorithm>

void usage_and_exit (char* exe) {
    std::cerr << "Usage: " << exe << " [options]\n" << std::endl;
    std::cerr << "Options:\n" << std::endl;
    std::cerr << "  -a <value>\tAnharmonic prefactor (Default: 0.0)" << std::endl;
    std::cerr << "  -b <value>\tbeta (Default: 1)" << std::endl;
    std::cerr << "  -d <value>\tdelta tau (Default: 0.1)" << std::endl;
    std::cerr << "  -k <value>\tNumber of cumulants (Default: 4)" << std::endl;
    std::cerr << "  -n <value>\tNumber of oscillators (Default: 4)" << std::endl;
    std::cerr << "  -p \t\tPrint sampled values" << std::endl;
    std::cerr << "  -r <value>\tNumber of realizations (Default: 100)" << std::endl;
    std::cerr << "  -v \t\tVerbose mode" << std::endl;
    std::cerr << "  -w <value>\tomega_0 (Default: 1)" << std::endl;
    std::cerr << "  -h \t\tPrint this help and exit" << std::endl;
    exit (1);
}

int main(int argc, char **argv)
{
// Parsing Arguments
    int c;
    double anh = 0.0;
    double beta = 1.0;
    double dtau = 0.1;
    size_t ksize = 4;
    const size_t kMAX = 6;
    size_t N = 4;
    double omega0 = 1;
    bool print = false;
    size_t realizations = 100;
    bool verbose = false;

    while ((c = getopt (argc, argv, "a:b:d:k:n:pr:vw:h")) != -1){
	switch (c)
	{
	    case 'a':
		if (! atof (optarg))
		    usage_and_exit(argv[0]);
		anh = std::atof (optarg);
		break;
	    case 'b':
		if (! atof (optarg))
		    usage_and_exit(argv[0]);
		beta = std::atof (optarg);
		break;
	    case 'd':
		if (! atof (optarg))
		    usage_and_exit(argv[0]);
		dtau = std::atof (optarg);
		break;
	    case 'k':
		if (! atof (optarg))
		    usage_and_exit(argv[0]);
		ksize = std::atoi (optarg);
		break;
	    case 'n':
		if (! atof (optarg))
		    usage_and_exit(argv[0]);
		N = std::atoi (optarg);
		break;
	    case 'p':
		print = true;
		break;
	    case 'r':
		if (! atof (optarg))
		    usage_and_exit(argv[0]);
		realizations = std::atol (optarg);
		break;
	    case 'v':
		verbose = true;
		break;
	    case 'w':
		if (! atof (optarg))
		    usage_and_exit(argv[0]);
		omega0 = std::atof (optarg);
		break;
	    case 'h':
		usage_and_exit(argv[0]);
		break;
	    case '?':
		usage_and_exit(argv[0]);
		break; // avoids warning on fallthrough since C++17
	    default:
		usage_and_exit(argv[0]);
	}
    }

    if (ksize < 1 || ksize > kMAX) {
	std::cerr << ksize << " is an invalid number of cumulants. Should be between 1 and " << kMAX << std::endl;
	exit(1);
    } else if (verbose)
        std::cerr << "# Up to " << ksize << " cumulants" << std::endl;

    AChain chain(beta, N, verbose, dtau, omega0, anh);
    if (verbose)
        std::cerr << "# Doing " << realizations << " realizations..." << std::endl;

    const int M = beta/dtau;
    RVector samples;
    if (ksize > 4)
	samples.resize(realizations);
    RVector mu(ksize); // moments
    double weight = 0;

    for (size_t iter = 0; iter < realizations; ++iter) {
	chain.init_A();
	chain.build_paths_for_A();
	const double new_ds = -dtau * chain.calc_Ep_anh() * M;
	const double new_w = exp(new_ds);
	weight += (new_w - weight) / (iter+1);

	const size_t n = iter+1;
	const double delta = new_ds - mu[0];
	const double delta_n = delta / n;
	mu[0] += delta_n;

	// https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
	if (ksize < 5) {
	    const double term1 = delta * delta_n * iter;
	    if (ksize > 3) {
		const double delta_n2 = delta_n * delta_n;
		mu[3] += term1 * delta_n2 * (n*n - 3*n + 3) + 6 * delta_n2 * mu[1] - 4 * delta_n * mu[2];
	    }
	    if (ksize > 2)
		mu[2] += term1 * delta_n * (n - 2) - 3 * delta_n * mu[1];
	    if (ksize > 1)
		mu[1] += term1;
	} else
	    samples[iter] = new_ds;

	if (print)
	    std::cout << new_ds << '\n';
    }

    std::cout << "# Sampled: log <exp(ds)> = " << log(weight) << "\t <exp(ds)> = " << weight << '\n';

    if (ksize < 5) {
	for (size_t m = 0; m < ksize; ++m) {
	    if (m>0)
		mu[m] /= realizations;
	    std::cout << "# m_" << m+1 << " = " << mu[m] << '\n';
	}
    } else {
	const double mean = mu[0];
	std::for_each(samples.begin(), samples.end(), [=](double &s){ s -= mean; });

	for (const auto s : samples)
	    for (size_t m = 1; m < ksize; ++m)
		mu[m] += pow(s,m+1) / realizations;

	for (size_t m = 0; m < ksize; ++m)
	    std::cout << "# m_" << m+1 << " = " << mu[m] << '\n';
    }

    RVector k(ksize); // cumulants
    k[0] = mu[0];
    k[1] = mu[1];
    if (ksize > 2)
	k[2] = mu[2];
    if (ksize > 3)
	k[3] = mu[3] - 3 * mu[1]*mu[1];
    if (ksize > 4)
	k[4] = mu[4] - 10 * mu[2]*mu[1];
    if (ksize > 5)
	k[5] = mu[5] - 15 * mu[3]*mu[1] - 10 * mu[2]*mu[2] + 30 * mu[1]*mu[1]*mu[1];

    for (size_t m = 0; m < ksize; ++m)
	std::cout << "# k_" << m+1 << " = " << k[m] << '\n';

    double estim = 0;
    for (size_t m = 0; m < ksize; ++m) {
	estim += k[m] / tgamma(m+2); // tgamma(m+2) == (m+1)!
	std::cout << "# Sum of " << m+1 << " cumulants: " << estim << '\n'; 
    }
    std::cout << "# Cumulants: log <exp(ds)> = " << estim << "\t <exp(ds)> = " << exp(estim) << '\n';
    return 0;
}
