/*  Path Integral Monte Carlo methods for a chain of coupled oscillators
    Copyright (C) 2021 Purrello V. H.

    This file is part of PIMC_Chain.

    PIMC_Chain is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PIMC_Chain is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PIMC_Chain. If not, see <http://www.gnu.org/licenses/> */

#include "anharmonic_chain.h"
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <tuple>
#include <algorithm>
#include <string>
#include <highfive/H5File.hpp>

const std::string XGROUP ("xdata");
const std::string CGROUP ("cdata");
const std::string XDSET ("x");
const std::string CDSET ("k"); //Name of each corr function dataset, need to append k number

void usage_and_exit (char* exe)
{
    std::cerr << "Usage: " << exe << " [options]\n" << std::endl;
    std::cerr << "Options:\n" << std::endl;
    std::cerr << "  -a <value>\tAnharmonic prefactor (Default: 0.0)" << std::endl;
    std::cerr << "  -b <value>\tbeta (Default: 1)" << std::endl;
    std::cerr << "  -d <value>\tdelta tau (Default: 0.1)" << std::endl;
    std::cerr << "  -f <filename>\tOutput to filename.hdf5 file" << std::endl;
    std::cerr << "  -n <value>\tNumber of oscillators (Default: 4)" << std::endl;
    std::cerr << "  -s <value>\tNumber of MC steps (Default: 0)" << std::endl;
    std::cerr << "  -v \t\tVerbose mode" << std::endl;
    std::cerr << "  -w <value>\tomega_0 (Default: 1)" << std::endl;
    std::cerr << "  -z <value>\tBlock size for average MC steps (Default: 1)" << std::endl;
    std::cerr << "  -h \t\tPrint this help and exit" << std::endl;
    exit (1);
}

void create_hdf5_file (
	size_t N, size_t M, double beta, double dtau, double omega0, double anh, size_t blocksize,
	const IVector& k_for_C, const std::string& filename)
{
    // Create a new file using the default property lists.
    HighFive::File file(filename, HighFive::File::Overwrite);

    {
	HighFive::Attribute a = file.createAttribute<size_t>("N", HighFive::DataSpace::From(N));
	a.write(N);
    }
    {
	HighFive::Attribute a = file.createAttribute<size_t>("M", HighFive::DataSpace::From(M));
	a.write(M);
    }
    {
	HighFive::Attribute a = file.createAttribute<double>("beta", HighFive::DataSpace::From(beta));
	a.write(beta);
    }
    {
	HighFive::Attribute a = file.createAttribute<double>("dtau", HighFive::DataSpace::From(dtau));
	a.write(dtau);
    }
    {
	HighFive::Attribute a = file.createAttribute<double>("omega0", HighFive::DataSpace::From(omega0));
	a.write(omega0);
    }
    {
	HighFive::Attribute a = file.createAttribute<double>("anh", HighFive::DataSpace::From(anh));
	a.write(anh);
    }
    {
	HighFive::Attribute a = file.createAttribute<size_t>("blocksize", HighFive::DataSpace::From(blocksize));
	a.write(blocksize);
    }

    // Create and initialize group for x
    {
	HighFive::Group group = file.createGroup(XGROUP);
	// Create a dataspace with initial shape and max shape
	HighFive::DataSpace dataspace = HighFive::DataSpace({M, N}, {HighFive::DataSpace::UNLIMITED, N});
	// Use chunking
	HighFive::DataSetCreateProps props;
	props.add(HighFive::Chunking(std::vector<hsize_t>{M, N}));
	// Create a dataset for each realization
	std::string datasetname(XDSET);
	HighFive::DataSet dataset = group.createDataSet(datasetname, dataspace,
							 HighFive::AtomicType<double>(), props);
    }
    // Create and initialize group for the real and imag part of the corr function
    {
	HighFive::Group group = file.createGroup(CGROUP);
	// Create a dataspace with initial shape and max shape
	HighFive::DataSpace dataspace = HighFive::DataSpace({1, M}, {HighFive::DataSpace::UNLIMITED, M});
	// Use chunking
	HighFive::DataSetCreateProps props;
	props.add(HighFive::Chunking(std::vector<hsize_t>{1, M}));
	// Create a dataset for each realization
	for (const auto &k : k_for_C) {
	    std::string realname(CDSET + std::to_string(k));
	    HighFive::DataSet realdataset = group.createDataSet(realname, dataspace,
							     HighFive::AtomicType<double>(), props);
	    std::string imagname(CDSET + std::to_string(k) + "_imag");
	    HighFive::DataSet imagdataset = group.createDataSet(imagname, dataspace,
							     HighFive::AtomicType<double>(), props);
	}
    }
}

void append_to_hdf5_file (
	size_t index, const RMatrix& xdata,
	const IVector& k_for_C, const CMatrix& cdata, size_t N, size_t M, const std::string& filename)
{
    // Open file
    HighFive::File file(filename, HighFive::File::ReadWrite);
    {
	HighFive::Group group = file.getGroup(XGROUP);
	// Get dataset
	std::string datasetname(XDSET);
	HighFive::DataSet dataset = group.getDataSet(datasetname);
	// Resize the dataset to a larger size
	dataset.resize({(index+1)*M, N});
	// Write into the new part of the dataset
	dataset.select({index*M, 0}, {M, N}).write(xdata);
    }
    {
	HighFive::Group group = file.getGroup(CGROUP);
	for (size_t ik = 0; ik < k_for_C.size(); ++ik) {
	    const size_t k = k_for_C[ik];
	    // Get datasets
	    std::string realname(CDSET + std::to_string(k));
	    HighFive::DataSet realdataset = group.getDataSet(realname);
	    std::string imagname(CDSET + std::to_string(k) + "_imag");
	    HighFive::DataSet imagdataset = group.getDataSet(imagname);
	    // Resize the dataset to a larger size
	    realdataset.resize({index+1, M});
	    imagdataset.resize({index+1, M});
	    // Write into the new part of the dataset
	    RVector realpart(M);
	    RVector imagpart(M);
	    for (size_t m = 0; m < M; ++m) {
		realpart[m] = cdata[m][ik].real();
		imagpart[m] = cdata[m][ik].imag();
	    }
	    realdataset.select({index, 0}, {1, M}).write(realpart);
	    imagdataset.select({index, 0}, {1, M}).write(imagpart);
	}
    }
}

int main(int argc, char **argv)
{
// Parsing Arguments
    int c;
    double anh = 0.0;
    double beta = 1.0;
    double dtau = 0.1;
    size_t N = 4;
    double omega0 = 1;
    size_t MCsteps = 0;
    size_t blocksize = 1;
    bool verbose = false;
    std::string filename("");

    while ((c = getopt (argc, argv, "a:b:d:f:n:s:vw:z:h")) != -1){
	switch (c)
	{
	    case 'a':
		if (! atof (optarg))
		    usage_and_exit(argv[0]);
		anh = std::atof (optarg);
		break;
	    case 'b':
		if (! atof (optarg))
		    usage_and_exit(argv[0]);
		beta = std::atof (optarg);
		break;
	    case 'd':
		if (! atof (optarg))
		    usage_and_exit(argv[0]);
		dtau = std::atof (optarg);
		break;
	    case 'f':
		filename.append(optarg);
		if (filename.empty())
		    usage_and_exit(argv[0]);
		break;
	    case 'n':
		if (! atof (optarg))
		    usage_and_exit(argv[0]);
		N = std::atol (optarg);
		break;
	    case 's':
		if (! atof (optarg))
		    usage_and_exit(argv[0]);
		MCsteps = std::atol (optarg);
		break;
	    case 'v':
		verbose = true;
		break;
	    case 'w':
		if (! atof (optarg))
		    usage_and_exit(argv[0]);
		omega0 = std::atof (optarg);
		break;
	    case 'z':
		if (! atof (optarg))
		    usage_and_exit(argv[0]);
		blocksize = std::atol (optarg);
		break;
	    case 'h':
		usage_and_exit(argv[0]);
		break;
	    case '?':
		usage_and_exit(argv[0]);
		break; // avoids warning on fallthrough since C++17
	    default:
		usage_and_exit(argv[0]);
	}
    }

    const size_t number_of_blocks = MCsteps / blocksize;
    if (MCsteps%blocksize != 0)
	std::cerr << "#MCsteps\%blocksize != 0, number of MC steps will be rounded to "
	    << number_of_blocks * blocksize
	    << std::endl;
    const size_t M = beta/dtau;
    const size_t systemsize = M * (N-1);
    bool OUTPUT_TO_FILE = false;
    if (! filename.empty()) {
	OUTPUT_TO_FILE = true;
	filename.append(".hdf5");
    }

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> rng_int_0M(0, M-1);
    std::uniform_int_distribution<> rng_int_1N(1, N-1);
    std::uniform_real_distribution<> rng_real_01(0.0, 1.0);

    AChain chain(beta, N, verbose, dtau, omega0, anh);
    const IVector k_for_C = chain.get_k_for_corr();
    if (OUTPUT_TO_FILE)
	create_hdf5_file (N, M, beta, dtau, omega0, anh, blocksize, k_for_C, filename);
    chain.init_A();
    chain.build_paths_for_A();
    auto Eold = chain.get_Ep_anh() * M;
    size_t acceptances = 0;
    for (size_t block = 0; block < number_of_blocks; ++block) {
	for (size_t step = 0; step < blocksize; ++step) {
	    for (size_t substep = 0; substep < systemsize; ++substep) {
		const size_t m = rng_int_0M(gen);
		size_t k = rng_int_1N(gen);
		size_t i = 0;
		if (k > N/2) {
		    k = N-k;
		    i = 1;
		}
		const auto Aold = chain.get_A(m, k, i);
		chain.direct_sampling_new_A(m, k, i);
		const auto Enew = chain.get_Ep_anh() * M;
		const auto exp_dE = exp(-dtau*(Enew-Eold));
		if (exp_dE > rng_real_01(gen)) {
		    Eold = Enew;
		    ++acceptances;
		}
		else {
		    chain.set_A(m, k, i, Aold);
		}
	    }
	    chain.update_mean_Ep_and_corr();
	}
	if (verbose) {
	    //chain.print_mean_corr();
	    chain.print_mean_Ep();
	}
	if (OUTPUT_TO_FILE)
	    append_to_hdf5_file(block, chain.get_x(), k_for_C, chain.get_mean_corr(), N, M, filename);
	chain.reset_accumulators();
    }
    if (verbose)
	std::cerr << "#Acceptance: "
	    << std::fixed << std::setw(3) << std::setprecision(3)
	    << (double)acceptances/(MCsteps*systemsize) * 100 << "%\n";

    return 0;
}
