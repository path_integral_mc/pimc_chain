/*  Path Integral Monte Carlo methods for a chain of coupled oscillators
    Copyright (C) 2021 Purrello V. H.

    This file is part of PIMC_Chain.

    PIMC_Chain is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PIMC_Chain is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PIMC_Chain. If not, see <http://www.gnu.org/licenses/> */

#include "chain.h"
#include <iostream>
#include <unistd.h>
#include <tuple>

void usage_and_exit (char* exe) {
    std::cerr << "Usage: " << exe << " [options]\n" << std::endl;
    std::cerr << "Options:\n" << std::endl;
    std::cerr << "  -b <value>\tbeta (Default: 1)" << std::endl;
    std::cerr << "  -d <value>\tdelta tau (Default: 0.1)" << std::endl;
    std::cerr << "  -n <value>\tNumber of oscillators (Default: 4)" << std::endl;
    std::cerr << "  -v \t\tVerbose mode" << std::endl;
    std::cerr << "  -w <value>\tomega_0 (Default: 1)" << std::endl;
    std::cerr << "  -h \t\tPrint this help and exit" << std::endl;
    exit (1);
}

int main(int argc, char **argv)
{
// Parsing Arguments
    int c;
    double beta = 1.0;
    double dtau = 0.1;
    size_t N = 4;
    double omega0 = 1;
    bool verbose = false;

    while ((c = getopt (argc, argv, "b:d:n:vw:h")) != -1)
	switch (c)
	{
	    case 'b':
		if (! atof (optarg))
		    usage_and_exit(argv[0]);
		beta = std::atof (optarg);
		break;
	    case 'd':
		if (! atof (optarg))
		    usage_and_exit(argv[0]);
		dtau = std::atof (optarg);
		break;
	    case 'n':
		if (! atof (optarg))
		    usage_and_exit(argv[0]);
		N = std::atoi (optarg);
		break;
	    case 'v':
		verbose = true;
		break;
	    case 'w':
		if (! atof (optarg))
		    usage_and_exit(argv[0]);
		omega0 = std::atof (optarg);
		break;
	    case 'h':
		usage_and_exit(argv[0]);
		break;
	    case '?':
		usage_and_exit(argv[0]);
		break; // avoids warning on fallthrough since C++17
	    default:
		usage_and_exit(argv[0]);
	}

    Chain chain(beta, N, verbose, dtau, omega0);
    chain.init_A();
    chain.build_paths_for_A();
    std::cout << "A:" << std::endl;
    chain.print_A();

    chain.A_to_x();
    std::cout << "x:" << std::endl;
    chain.print_x();

    chain.x_to_A();
    std::cout << "A:" << std::endl;
    chain.print_A();

    if (verbose) {
	const auto Ep = chain.calc_Ep();
	const auto Ep_from_A = chain.calc_Ep_from_A();
	std::cerr << "Ep = " << Ep << '\t' << "Ep_from_A = " << Ep_from_A << std::endl;
    }
    return 0;
}
